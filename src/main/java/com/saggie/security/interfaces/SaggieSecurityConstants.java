package com.saggie.security.interfaces;

import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;


public interface SaggieSecurityConstants {
    List<GrantedAuthority> NO_AUTHORITIES = new ArrayList(1);
    String SAGGIE_ACCESS_TOKEN = "saggie-access-token";
    String AUTHORIZATION_HEADER = "Authorization";
    String BEARER = "Bearer";
    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_AGENT = "ROLE_AGENT";
    String ROLE_LEADER = "ROLE_LEADER";
}
