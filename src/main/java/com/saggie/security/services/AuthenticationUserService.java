/**
 * Copyright (c) 2016, AskLytics and/or its affiliates. All rights reserved.
 * <p/>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p/>
 * Created by bilalshah on 21/04/2016
 */
package com.saggie.security.services;


import com.saggie.data.access.entities.UserEntity;
import com.saggie.data.access.repositories.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
public class AuthenticationUserService implements UserService {
    private static Logger logger = LogManager.getLogger(AuthenticationUserService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserEntity getUser(String username) {
        UserEntity user = userRepository.findByEmail(username);
        if (user != null) {
            return user;
        }
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(String username) {
        return Collections.emptyList();
    }

}
