/**
 * Copyright (c) 2016, AskLytics and/or its affiliates. All rights reserved.
 * <p/>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p/>
 * Created by bilalshah on 21/04/2016
 */
package com.saggie.security.services;


import com.saggie.data.access.entities.UserEntity;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public interface UserService {
    UserEntity getUser(String username);

    Collection<? extends GrantedAuthority> getAuthorities(String username);
}
