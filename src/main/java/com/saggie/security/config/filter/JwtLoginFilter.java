package com.saggie.security.config.filter;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import com.saggie.commons.ws.enums.ApiResponseStatus;
import com.saggie.commons.ws.logger.SaggieLogger;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.utils.JSONUtils;
import com.saggie.commons.ws.utils.JavaUtils;
import com.saggie.commons.ws.utils.SaggieCollectionUtils;
import com.saggie.commons.ws.utils.SaggieObjectUtils;
import com.saggie.security.exception.ExpiredTokenException;
import com.saggie.security.interfaces.SaggieSecurityConstants;
import com.saggie.security.jwt.JwtUtils;
import com.saggie.security.models.AuthentificationResponse;
import com.saggie.security.user.SaggieUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {
    private static final Logger log = LogManager.getLogger(SecurityResponseUtils.class);

    @Autowired
    private CookieService cookieService;
    @Autowired
    private JwtUtils jwtUtils;
    private static final String AUTHENTICATION_SCHEME = "Bearer";

    public JwtLoginFilter(String url, AuthenticationManager authManager, AuthenticationFailureHandler authenticationFailureHandler) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        setAuthenticationFailureHandler(authenticationFailureHandler);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException {
        SaggieLogger.log(log, Level.DEBUG, "createAuthenticationToken");
        String authToken = "";
        Cookie[] cookies = req.getCookies();
        JwtAuthenticationRequest creds = null;

        if(JavaUtils.isEmpty(authToken) && SaggieCollectionUtils.isNotEmpty(cookies)){
            Cookie tokenCookie = Arrays.stream(cookies).
                    filter(cookie -> cookie.getName().equalsIgnoreCase(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN)).findFirst().orElse(null);
            if(tokenCookie != null )
                authToken = tokenCookie.getValue();
        }
        if (JavaUtils.isNotEmpty(authToken)) {
            try {
                Claims claims = jwtUtils.parseUsernameFromToken(authToken);
                SaggieUser asklyticsUser = jwtUtils.getSaggieUserFromToken(claims);
                Authentication authenticationResult = new UsernamePasswordAuthenticationToken(asklyticsUser, asklyticsUser.getPassword(), asklyticsUser.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authenticationResult);

                return authenticationResult;
            } catch (JwtException e) {
                logger.error("Not able to validate token. Checking for username/password");
                if (e instanceof ExpiredJwtException) {
                    creds = new ObjectMapper().readValue(req.getInputStream(), JwtAuthenticationRequest.class);
                    if(SaggieObjectUtils.isNull(creds)) {
                        logger.error("Token is expired " + e);
                        throw new ExpiredTokenException("Token in the cookie is expired");
                    }
                } if (e instanceof MalformedJwtException){
                    creds = new ObjectMapper().readValue(req.getInputStream(), JwtAuthenticationRequest.class);
                    if(SaggieObjectUtils.isNull(creds)) {
                        logger.error("Token is invalid");
                        throw new InvalidCookieException("Token is not valid in the cookie");
                    }

                }
            }
        }


        //normal user password authentication
        if (SaggieObjectUtils.isNull(creds)) {
            creds = new ObjectMapper().readValue(req.getInputStream(), JwtAuthenticationRequest.class);
        }
        final Authentication authentication = getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        creds.getUsername(),
                        creds.getPassword(),
                        Collections.emptyList()
                )
        );

        User user = (User) authentication.getPrincipal();
        if (user instanceof SaggieUser) {
            final SaggieUser userDetails = (SaggieUser) authentication.getPrincipal();
            if (userDetails.getPasswordExpired() != null && userDetails.getPasswordExpired()) {
                //extract reason code by tokenize with delimiter (,) e.g password expired, 1 here 1 is the expiry reason
                throw new CredentialsExpiredException("Password Expired.");
            }
            if(!userDetails.isAccountNonLocked()){
                throw new LockedException("Account Locked");
            }
            if (!userDetails.isEnabled()){
                throw new DisabledException("Account Suspended");
            }
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return authentication;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res,
                                            FilterChain chain, Authentication auth) throws IOException, ServletException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user instanceof SaggieUser) {
            // Reload password post-filter so we can generate token
            final SaggieUser userDetails = (SaggieUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

            final String token = jwtUtils.createTokenForUser(
                    userDetails.getUserId(),
                    userDetails.getUsername(),
                    userDetails.getLastName(),
                    userDetails.getFirstName(),
                    roles.toArray(new String[roles.size()])
            );

            SaggieLogger.log(log, Level.INFO, "createAuthenticationToken " +  userDetails.getUserId());

            AuthentificationResponse authentificationResponse = new AuthentificationResponse();
            authentificationResponse.setToken(token);
            ApiCommonResponseWs apiCommonResponseWs = new ApiCommonResponseWs();
            apiCommonResponseWs.setStatus(ApiResponseStatus.SUCCESS);
            apiCommonResponseWs.setApiContext(ApiContext.AUTHENTIFICATION);
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.NONE);
            apiCommonResponseWs.setData(authentificationResponse);
            String json = JSONUtils.toJSONWithoutClassName(apiCommonResponseWs);
            res.setContentType("application/json");
            res.setCharacterEncoding("UTF-8");
            PrintWriter out = res.getWriter();
            out.print(json);
            out.flush();

            res.addCookie(cookieService.setAlAccessTokenCookie(token));
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();

        if (logger.isDebugEnabled()) {
            logger.debug("Authentication request failed: " + failed.toString(), failed);
            logger.debug("Updated SecurityContextHolder to contain null Authentication");
            logger.debug("Delegating to authentication failure handler " + getFailureHandler());
        }

        getFailureHandler().onAuthenticationFailure(request, response, failed);
    }

}
