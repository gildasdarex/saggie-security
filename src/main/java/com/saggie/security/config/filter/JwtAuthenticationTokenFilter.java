package com.saggie.security.config.filter;

import com.saggie.commons.ws.logger.SaggieLogger;
import com.saggie.commons.ws.utils.JavaUtils;
import com.saggie.commons.ws.utils.SaggieCollectionUtils;
import com.saggie.security.authentification.SaggieUserDetailsService;
import com.saggie.security.interfaces.SaggieSecurityConstants;
import com.saggie.security.jwt.JwtUtils;
import com.saggie.security.utils.CommonUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class JwtAuthenticationTokenFilter extends GenericFilterBean {
    private static final Logger log = LogManager.getLogger(JwtAuthenticationTokenFilter.class);

    @Autowired
    private JwtUtils jwtUtils;

    @Value("${app.exclude.url:/v1/users/admin}") //default
    private String appExcludeUrl;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;
    @Autowired
    private CookieService cookieService;
    @Autowired
    private SaggieUserDetailsService saggieUserDetailsService;

    public JwtAuthenticationTokenFilter() {
    }

    public JwtAuthenticationTokenFilter(JwtUtils jwtUtils, AuthenticationEntryPoint authenticationEntryPoint, CookieService cookieService, SaggieUserDetailsService saggieUserDetailsService) {
        this.jwtUtils = jwtUtils;
        this.cookieService = cookieService;
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.saggieUserDetailsService = saggieUserDetailsService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String authToken = "";
        Cookie[] cookies = request.getCookies();


        if(! excludeUrl(request.getRequestURI())){
            if (JavaUtils.isEmpty(authToken) && SaggieCollectionUtils.isNotEmpty(cookies)) {
                Cookie tokenCookie = Arrays.stream(cookies).
                        filter(cookie -> cookie.getName().equalsIgnoreCase(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN)).findFirst().orElse(null);
                if (tokenCookie != null)
                     authToken = tokenCookie.getValue();
            }

            if (JavaUtils.isEmpty(authToken)) {
                log.warn("security error: Attempting to access restricted resource without access token in cookie. " +
                        "InvalidCookieException(\"Cookie Does not contain access token\") HttpSeveletRequest dump is below\n" +
                        CommonUtils.extractHttpRequestData(request, new StringBuilder()).toString());
                authenticationEntryPoint.commence(request, response, new InvalidCookieException("Cookie does not contain access token"));
                return;
            }

            // authentication token exists
            try {
                Claims claims = jwtUtils.parseUsernameFromToken(authToken);
                if (claims != null) {
                    String username = (String) claims.get(JwtUtils.CLAIM_KEY_USERNAME);

                    SaggieLogger.log(log, Level.INFO, "checking authentication for user " + username);

                    if (username != null && !username.isEmpty() && SecurityContextHolder.getContext().getAuthentication() == null) {

                        authToken = jwtUtils.refreshToken(authToken);
                        // It is not compelling necessary to load the use details from the database. You could also store the information
                        // in the token and read it from it. It's up to you ;)
                        UserDetails userDetails = jwtUtils.getSaggieUserFromToken(claims);

                        // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
                        // the database compellingly. Again it's up to you ;)
                        if (jwtUtils.validateToken(authToken, userDetails)) {
                            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            logger.info("authenticated user " + username + ", setting filter context");
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                            Cookie refreshedCookie = cookieService.updateCookie(request.getCookies(), SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN, authToken);
                            response.addCookie(refreshedCookie);
                            //authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
                        }
                    }
                }
            } catch (JwtException e) {
                CommonUtils.postAuthenticationException(authenticationEntryPoint, request, response, authToken, e);
                return;
            }
        }
        logger.info(request.getRequestURI());
        chain.doFilter(request, response);
    }


    private Boolean excludeUrl(String url){
        logger.info(url);
        String [] appExcludeUrlAsArray = appExcludeUrl.split(",");
        String [] items = new String[]{
                "/",
                ".*(home).*",
                ".*(/alprometheus).*",
                "^.*(/logout).*",
                ".*(/v2/api-docs).*",
                ".*(/configuration).*",
                ".*(/swagger*).*",
                ".*(/webjars).*"
        };
        String[] excludeUrls = ArrayUtils.addAll(appExcludeUrlAsArray, items);

        for(String item : excludeUrls){
            //match with regex not inverse .. it will fail.
            logger.info(item);
            logger.info(url.matches(item));
            if(url.matches(item)) return true;
        }

        return false;
    }

}