package com.saggie.security.config.filter;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SaggieAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    SecurityResponseUtils securityResponseUtils;

    public SaggieAuthenticationFailureHandler(SecurityResponseUtils securityResponseUtils) {
        this.securityResponseUtils = securityResponseUtils;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
            logger.debug("No failure URL set, sending 401 Unauthorized error");
            response.getWriter().write(securityResponseUtils.getAuthenticationResponse(exception));
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

}
