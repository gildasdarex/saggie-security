package com.saggie.security.config.filter;

import com.saggie.commons.ws.utils.JSONUtils;
import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import com.saggie.commons.ws.enums.ApiResponseStatus;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.security.exception.ExpiredTokenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;

public class SecurityResponseUtils {
    private static final Logger logger = LogManager.getLogger(SecurityResponseUtils.class);

    String apiVersion;

    public SecurityResponseUtils(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getAuthenticationResponse(AuthenticationException authentication){
        ApiCommonResponseWs apiCommonResponseWs = new ApiCommonResponseWs();
        apiCommonResponseWs.setStatus(ApiResponseStatus.FAILURE);
        apiCommonResponseWs.setApiContext(ApiContext.AUTHENTIFICATION);

        if (authentication instanceof BadCredentialsException) {
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.BAD_CREDENTIALS);
        } else if (authentication instanceof ExpiredTokenException){
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.TOKEN_EXPIRED);
        } else if (authentication instanceof InvalidCookieException) {
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.TOKEN_INVALID);
        }
        else if (authentication instanceof LockedException) {
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.USER_DEACTIVATED);
        }
        else if (authentication instanceof DisabledException){
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.ACCOUNT_SUSPENDED);
        }
        else if (authentication instanceof CredentialsExpiredException){
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.NONE);
        } else {
            apiCommonResponseWs.setApiResponseErrorCategory(ApiResponseErrorCategory.AUTHENTICATION_ERROR);
        }

        return JSONUtils.toJSONWithoutClassName(apiCommonResponseWs);
    }
}
