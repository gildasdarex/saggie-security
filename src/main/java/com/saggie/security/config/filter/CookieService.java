package com.saggie.security.config.filter;

import com.saggie.commons.ws.utils.SaggieObjectUtils;
import com.saggie.security.interfaces.SaggieSecurityConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;

@Component
public class CookieService {

    @Value("${app.cookie.path:/}") //default /
    String path;

    @Value("${app.cookie.httpOnly:false}") //default false
    Boolean httpOnly;

    @Value("${app.cookie.maxAge:-1}") //default -1
    Integer maxAge;

    @Value("${app.cookie.secure:false}") //default false
    Boolean secure;

    @Value("${app.cookie.domain:localhost}") //default localhost
    String domain;

    public Cookie setAlAccessTokenCookie(String authToken){
        // Return the token
        return setCookie(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN, authToken);
    }

    private Cookie setCookie(String name, String value){
        return setCookie(name, value, path, domain, secure, httpOnly, maxAge);
    }

    public Cookie logoutTokenCookie(){
        // Return the token
        Cookie newCookie = new Cookie(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN, "");
        newCookie.setPath(path);
        //newCookie.setHttpOnly(httpOnly);
        newCookie.setMaxAge(0);
        newCookie.setSecure(secure);
        newCookie.setDomain(domain);
//        if(!Strings.isNullOrEmpty(domain)){
//            newCookie.setDomain(domain);
//        }
        return newCookie;
    }

    public Cookie updateCookie(Cookie[] cookies, String name, String value){
        if (SaggieObjectUtils.isNull(cookies))
            cookies = new Cookie[]{};

        for (Cookie cookie : cookies){
            if (cookie.getName().equals(name)){
                return setCookie(name, value, path, domain, secure, httpOnly, maxAge);
            }
        }
        return setCookie(name, value);
    }

    private Cookie setCookie(String name, String value, String path, String domain, boolean secure, boolean httpOnly, int maxAge){
        // Return the token
        Cookie newCookie = new Cookie(name, value);
        newCookie.setPath(path);
        //newCookie.setHttpOnly(httpOnly);
        newCookie.setMaxAge(maxAge);
        newCookie.setSecure(secure);
        newCookie.setDomain(domain);
//        if(!Strings.isNullOrEmpty(domain)){
//            newCookie.setDomain(domain);
//        }
        return newCookie;
    }

    public CookieService() {
    }

    public CookieService(String path, String domain, boolean secure, boolean httpOnly, int maxAge) {
        this.path = path;
        this.domain = domain;
        this.maxAge = maxAge;
        this.secure = secure;
        this.httpOnly = httpOnly;
    }
}
