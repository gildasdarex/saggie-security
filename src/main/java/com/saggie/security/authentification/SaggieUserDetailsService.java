package com.saggie.security.authentification;


import com.saggie.data.access.entities.UserEntity;
import com.saggie.security.interfaces.SaggieSecurityConstants;
import com.saggie.security.services.UserService;
import com.saggie.security.user.SaggieUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component("saggieUserDetailsService")
public class SaggieUserDetailsService implements UserDetailsService, SaggieSecurityConstants {

    @Autowired
    private UserService userService;


    public UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {
        UserEntity user = userService.getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found : " + username);
        } else {
            Collection<? extends GrantedAuthority> authorities = userService.getAuthorities(username);
            if (loadRoles)
                return new SaggieUser(user, (authorities.isEmpty() ? NO_AUTHORITIES : authorities));
            else
                return new SaggieUser(user, NO_AUTHORITIES);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loadUserByUsername(username, true);
    }
}
