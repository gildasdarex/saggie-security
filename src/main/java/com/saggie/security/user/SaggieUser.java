/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/27/18
 */
package com.saggie.security.user;

import com.saggie.data.access.entities.UserEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SaggieUser extends User {
    private static Logger logger = LogManager.getLogger(SaggieUser.class);


    private Integer userId;
    private List<Integer> roleIds;
    private String firstName;
    private String lastName;
    private String email;
    private Long lastLogin;
    private Boolean isPasswordExpired;

    public SaggieUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public SaggieUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public SaggieUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer userId, String firstName, String lastName, Long lastLogin) {
        super(username, password, authorities);
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastLogin = lastLogin;
    }

    public SaggieUser(UserEntity user, Collection authorities){
       this(user.getEmail(), user.getPassword(), Boolean.TRUE, true, true,
                Boolean.TRUE, authorities);

        userId = user.getId();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
/*
        List<SaggieRole> roles = user.getRoles();

        if(user.getLastLogin()!= null){
            lastLogin = user.getLastLogin().getTime();
        }*/

        roleIds = new ArrayList<>();
        roleIds.add(user.getRoleId());
        isPasswordExpired = Boolean.FALSE;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<Integer> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Integer> roleIds) {
        this.roleIds = roleIds;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Boolean getPasswordExpired() {
        return isPasswordExpired;
    }

    public void setPasswordExpired(Boolean passwordExpired) {
        isPasswordExpired = passwordExpired;
    }
}
