/**
 * Copyright (c) 2017, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 08/09/2017
 */
package com.saggie.config.filter;

import com.saggie.security.authentification.SaggieUserDetailsService;
import com.saggie.security.config.filter.CookieService;
import com.saggie.security.config.filter.JwtAuthenticationEntryPoint;
import com.saggie.security.config.filter.JwtAuthenticationTokenFilter;
import com.saggie.security.config.filter.SecurityResponseUtils;
import com.saggie.security.interfaces.SaggieSecurityConstants;
import com.saggie.security.jwt.JwtUtils;
import com.saggie.security.services.AuthenticationUserService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.Cookie;

public class JwtAuthenticationFilterTest {

    @InjectMocks
    SaggieUserDetailsService saggieUserDetailsService;

    @Mock
    AuthenticationUserService userService;

    @BeforeClass
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test(priority = 0)
    public void InvalidTokenOrEmptyTokenTest() throws Exception {
        JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter(new JwtUtils(), new JwtAuthenticationEntryPoint(new SecurityResponseUtils("1.0")), new CookieService(), saggieUserDetailsService);

        MockHttpServletRequest mockReq = new MockHttpServletRequest();
        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
        FilterConfig mockFilterConfig = Mockito.mock(FilterConfig.class);

        jwtAuthenticationTokenFilter.doFilter(mockReq, mockResp, mockFilterChain);
        Assert.assertEquals(mockResp.getStatus(), HttpStatus.UNAUTHORIZED.value());
        jwtAuthenticationTokenFilter.destroy();
    }

    @Test(priority = 0)
    public void tokenShouldNotExpireInBetween() throws Exception {
        JwtUtils jwtUtils = new JwtUtils();
        String originalToken = jwtUtils.createTokenForUser(11, "test@abc.com",

                "bond",
                "james",
                new String[]{"ADMIN", "CLERK"});
        CookieService cookieService = new CookieService("/", "localhost", false, false, -1);

        MockHttpServletRequest mockReq = new MockHttpServletRequest();
        mockReq.setCookies(cookieService.setAlAccessTokenCookie(originalToken));

        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
        FilterConfig mockFilterConfig = Mockito.mock(FilterConfig.class);

        JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter(new JwtUtils(), new JwtAuthenticationEntryPoint(new SecurityResponseUtils("1.0")), cookieService, saggieUserDetailsService);
        jwtAuthenticationTokenFilter.doFilter(mockReq, mockResp, mockFilterChain);
        Assert.assertEquals(mockResp.getStatus(), HttpStatus.OK.value());
        Assert.assertEquals(mockResp.getCookies().length, 1);
        Cookie cookie = mockResp.getCookie(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN);
        Assert.assertNotEquals(originalToken, cookie.getValue());
        jwtAuthenticationTokenFilter.destroy();
    }
}
