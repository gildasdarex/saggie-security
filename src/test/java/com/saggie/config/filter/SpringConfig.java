/**
 * Copyright (c) 2019, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 2019-09-01
 */
package com.saggie.config.filter;

import com.saggie.security.authentification.SaggieUserDetailsService;
import org.mockito.Mockito;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

@ComponentScan({"com.saggie.security"})
@ImportResource({"classpath:/spring-saggie-security.xml", "classpath:/spring-config.xml"})
@EnableWebSecurity
@Configuration
public class SpringConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        return Mockito.mock(SaggieUserDetailsService.class);
    }
}