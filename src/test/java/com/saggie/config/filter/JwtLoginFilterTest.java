package com.saggie.config.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saggie.data.access.entities.UserEntity;
import com.saggie.data.access.repositories.UserRepository;
import com.saggie.security.config.filter.JwtAuthenticationRequest;
import com.saggie.security.interfaces.SaggieSecurityConstants;
import com.saggie.security.services.AuthenticationUserService;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest(classes = SpringConfig.class)
@AutoConfigureMockMvc
@AutoConfigureWebMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class JwtLoginFilterTest  extends AbstractTestNGSpringContextTests {
    private static final String KNOWN_USER_MAIL = "valerian@imf.com";
    private static final String KNOWN_USER_PASSWORD = "pa$$w0rd";

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private AuthenticationUserService authenticationUserService;
    @Autowired
    private MockMvc mockMvc;

    @BeforeClass
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void successLoginTest() throws Exception{
        Mockito.when(userRepository.findByEmail(KNOWN_USER_MAIL)).thenReturn(createdUser(SaggieSecurityConstants.ROLE_ADMIN));
        Mockito.when(authenticationUserService.getUser(KNOWN_USER_MAIL)).thenReturn(createdUser(SaggieSecurityConstants.ROLE_ADMIN));
        Answer<Collection<? extends GrantedAuthority>> answers = (invocationOnMock) -> getRoles();

        Mockito.when(authenticationUserService.getAuthorities(KNOWN_USER_MAIL)).thenAnswer(answers);
        ObjectMapper objectMapper = new ObjectMapper();
        JwtAuthenticationRequest loginInfo = new JwtAuthenticationRequest(KNOWN_USER_MAIL, KNOWN_USER_PASSWORD);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/login")
                .content(objectMapper.writeValueAsString(loginInfo));

        mockMvc.perform(requestBuilder)
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.cookie().exists(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN));
    }

    @Test
    public void incorrectPassword() throws Exception{
        Mockito.when(authenticationUserService.getUser(KNOWN_USER_MAIL)).thenReturn(createdUser(SaggieSecurityConstants.ROLE_ADMIN));
        Answer<Collection<? extends GrantedAuthority>> answers = (invocationOnMock) -> getRoles();

        Mockito.when(authenticationUserService.getAuthorities(KNOWN_USER_MAIL)).thenAnswer(answers);
        ObjectMapper objectMapper = new ObjectMapper();
        JwtAuthenticationRequest loginInfo = new JwtAuthenticationRequest(KNOWN_USER_MAIL, "abcdef");

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/login")
                .content(objectMapper.writeValueAsString(loginInfo));

        mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    private static UserEntity createdUser(String userAccountRole){
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail("valerian@imf.com");
        userEntity.setFirstName("James");
        userEntity.setLastName("Bond");
        userEntity.setDateOfBirth(Timestamp.valueOf(LocalDate.of(1990, Month.APRIL, 18 ).atTime(0, 0)));
        userEntity.setPassword("pa$$w0rd");
        userEntity.setRoleId(1);
        userEntity.setId(1);
        return userEntity;
    }

    private static Collection<? extends GrantedAuthority> getRoles(){
        List<GrantedAuthority> authorities = new ArrayList<>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(SaggieSecurityConstants.ROLE_ADMIN);
        authorities.add(simpleGrantedAuthority);
        return authorities;
    }
}
